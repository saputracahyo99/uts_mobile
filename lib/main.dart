import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS Mobile',
      
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title:'UTS Mobile'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _nama = "";

  void _ubahnama() {
    setState(() {
      _nama = "CAHYO SAPUTRA" ;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        leading: new Icon(Icons.home),
        title: Text(widget.title),
        actions: <Widget>[
          new Icon(Icons.search),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Namaku Adalah : ', style: new TextStyle(fontSize:30.0),),
            Text('$_nama',style: new TextStyle(fontSize: 35.0),),
            RaisedButton  (child: Text("Ubah Nama",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed: _ubahnama,)
          ],
        ),
      ), 
    );
  }
}